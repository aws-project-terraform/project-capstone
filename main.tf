terraform {
  backend "s3" {
    bucket         = "project-capstone-devops-tfstate"
    key            = "recipe-app.tfstate"
    region         = "ap-northeast-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "ap-northeast-1"
  version = "~> 2.50.0"
}
